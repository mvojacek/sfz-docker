FROM mvojacek/build_rust-nightly-pinned:2020-04-18 as build

RUN cargo install sfz


FROM debian:stable-slim

WORKDIR /web
ENTRYPOINT /sfz -r -p80

COPY --from=build /usr/local/cargo/bin/sfz /sfz

